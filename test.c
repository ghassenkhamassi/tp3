#include <stdio.h>

void test_hasard() {
    char choix = hasard();
    if (choix == 'R' || choix == 'P' || choix == 'C') {
        printf("Test hasard: Pass\n");
    } else {
        printf("Test hasard: Fail\n");
    }
}

void test_comparaison() {
    // Test 1: Roche vs. Ciseaux, joueur gagne
    comparaison('R', 'C');
    
    // Test 2: Papier vs. Roche, joueur gagne
    comparaison('P', 'R');
    
    // Test 3: Ciseaux vs. Papier, joueur gagne
    comparaison('C', 'P');
    
    // Test 4: Roche vs. Roche, égalité
    comparaison('R', 'R');
    
    // Test 5: Papier vs. Ciseaux, ordinateur gagne
    comparaison('P', 'C');
}

int main() {
    test_hasard();
    test_comparaison();
    return 0;
}
