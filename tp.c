#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Fonction pour générer un choix aléatoire de l'ordinateur
char hasard() {
    int random_num = rand() % 3;
    if (random_num == 0)
        return 'R'; // Roche
    else if (random_num == 1)
        return 'P'; // Papier
    else
        return 'C'; // Ciseaux
}

// Fonction pour comparer les choix du joueur et de l'ordinateur
int comparaison(char joueur, char ordinateur) {
    if (joueur == ordinateur)
        return 0; // Égalité
    else if ((joueur == 'R' && ordinateur == 'C') || 
             (joueur == 'P' && ordinateur == 'R') || 
             (joueur == 'C' && ordinateur == 'P'))
        return 1; // Le joueur gagne
    else
        return -1; // L'ordinateur gagne
}

int main() {
    char choix_joueur;
    char choix_ordi;
    int resultat;

    srand(time(NULL)); // Initialisation de la graine pour rand()

    printf("Jouez au jeu Roche/Papier/Ciseaux!\n");
    printf("Choisissez R (Roche), P (Papier) ou C (Ciseaux) : ");
    scanf(" %c", &choix_joueur); // On utilise " %c" pour ignorer les espaces et les retours à la ligne

    if (choix_joueur != 'R' && choix_joueur != 'P' && choix_joueur != 'C') {
        printf("Choix invalide. Veuillez entrer R, P ou C.\n");
        return 1; // Quitte le programme avec un code d'erreur
    }

    choix_ordi = hasard();
    printf("L'ordinateur a choisi %c.\n", choix_ordi);

    resultat = comparaison(choix_joueur, choix_ordi);
    if (resultat == 0)
        printf("Égalité !\n");
    else if (resultat == 1)
        printf("Vous avez gagné !\n");
    else
        printf("L'ordinateur a gagné.\n");

    return 0; // Fin du programme
}
